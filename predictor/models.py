from sqlalchemy import Column, BigInteger, Integer, Float, String
from db import Base

class Punto(Base):
    __tablename__ = 'puntos'
    id = Column(BigInteger, primary_key=True)
    model_id = Column(Integer, unique=False)
    x_var = Column(Float, unique=False)
    y_var = Column(Float, unique=False)
    
    def __init__(self, model_id=None, x_var=None, y_var=None):
        self.model_id = model_id
        self.x_var = x_var
        self.y_var = y_var
    
    def getJson(self):
        json = {
            'id': self.id,
            'model_id': self.model_id,
            'x_var': self.x_var,
            'y_var': self.y_var
        }
        return json

    def __repr__(self):
        return "Punto: ({0},{1},{2})".format(self.model_id, self.x_var, self.y_var)