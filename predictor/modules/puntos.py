import functools
from flask import Blueprint, request, jsonify

from db import db_session
from models import Punto
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats

bp = Blueprint('puntos', __name__, url_prefix='/puntos')

@bp.route('/', methods=['POST'])
def create():
	model_id = request.form['model_id']
	x = request.form['x']
	y = request.form['y']
	punto = Punto(model_id, x, y)
	db_session.add(punto)
	db_session.commit()
	return jsonify(punto.getJson())

@bp.route('/estimar', methods=['GET'])
def estimar():
	model_id = request.args.get('model_id')
	x = request.args.get('x')
	y = estimar_valor(model_id, x)
	retorno = {
		'y': y
	}
	return jsonify(retorno)

def estimar_valor(model, x):
	data_x = []
	data_y = []
	cont = 0
	for row in db_session.query(Punto).filter_by(model_id = model):
		data_x.append(row.x_var)
		data_y.append(row.y_var)
	print(data_x)
	print(data_y)
	data_x_np = np.array(data_x)
	data_y_np = np.array(data_y)
	slope, intercept, r_value, p_value, std_err = stats.linregress(data_x_np, data_y_np)
	print(type(slope))
	print(type(intercept))
	print(type(x))
	return intercept + slope*float(x)




