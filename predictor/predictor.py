import os
from flask import Flask
from db import db_session

def create_app(test_config=None):
	app = Flask(__name__, instance_relative_config=True)
        
	@app.teardown_appcontext
	def shutdown_session(exception=None):
	    db_session.remove() 
	
	from modules.puntos import bp
	app.register_blueprint(bp)

	return app

   